package com.tricon.sanket.pocrecyclerview;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView mRVGalleryList;
    List<String> mImageList;
    private GalleryAdapter mGalleryAdapter;
    private StaggeredAdapter mStaggeredAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRVGalleryList = (RecyclerView) findViewById(R.id.rv_gallery_list);
        final StaggeredGridLayoutManager staggeredLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        //final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mRVGalleryList.setLayoutManager(staggeredLayoutManager);

        mImageList = new ArrayList<>();
        new fetchGalleryImages().execute();

        mGalleryAdapter = new GalleryAdapter(mImageList);
        mStaggeredAdapter = new StaggeredAdapter(mImageList);
        mRVGalleryList.setAdapter(mStaggeredAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_grid:
                final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
                mRVGalleryList.setLayoutManager(gridLayoutManager);
                mRVGalleryList.setAdapter(mGalleryAdapter);
                break;
            case R.id.menu_list:
                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                mRVGalleryList.setLayoutManager(linearLayoutManager);
                mRVGalleryList.setAdapter(mGalleryAdapter);
                break;
            default:
                final StaggeredGridLayoutManager staggeredLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                staggeredLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
                mRVGalleryList.setLayoutManager(staggeredLayoutManager);
                mRVGalleryList.setAdapter(mStaggeredAdapter);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    class fetchGalleryImages extends AsyncTask<Void, Void, List<String>>{

        @Override
        protected List<String> doInBackground(Void... params) {
            List<String> imageList = new ArrayList<>();

            final String[] originalImageProjection = {
                    MediaStore.Images.ImageColumns.DATA};

            Cursor imageCursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    originalImageProjection, null, null, null);

            if(imageCursor != null){
                while(imageCursor.moveToNext()){
                    String imageUrl = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
                    imageList.add(imageUrl);
                }
                imageCursor.close();
            }

            return imageList;
        }

        @Override
        protected void onPostExecute(List<String> imageList) {
            super.onPostExecute(imageList);
            mImageList.addAll(imageList);
            mGalleryAdapter.notifyDataSetChanged();
        }
    }
}
