package com.tricon.sanket.pocrecyclerview;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sanket on 09/02/2016.
 */
public class StaggeredAdapter extends RecyclerView.Adapter<StaggeredAdapter.ViewHolder> {

    List<String> mGalleryImageList;

    public StaggeredAdapter(List<String> imageList) {
        this.mGalleryImageList = imageList;
    }

    @Override
    public int getItemViewType(int position) {
        return (position % 2 == 0) ? 0 : 1;
    }

    @Override
    public StaggeredAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_staggered, parent, false));
        if(viewType==0){
            viewHolder.ivGalleryItem.setHeightRatio(0.8);
        } else {
            viewHolder.ivGalleryItem.setHeightRatio(1.6);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StaggeredAdapter.ViewHolder holder, int position) {
        String url = mGalleryImageList.get(position);
        Picasso.with(holder.ivGalleryItem.getContext())
                .load(TextUtils.isEmpty(url) ? null : "file://" + url)
                .centerCrop()
                .fit()
                .into(holder.ivGalleryItem);
    }

    @Override
    public int getItemCount() {
        return mGalleryImageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        DynamicHeightImageView ivGalleryItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ivGalleryItem = (DynamicHeightImageView) itemView.findViewById(R.id.iv_gallery_item);
        }
    }
}

