package com.tricon.sanket.pocrecyclerview;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by sanket on 08/02/2016.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    List<String> mGalleryImageList;

    public GalleryAdapter(List<String> imageList) {
        this.mGalleryImageList = imageList;
    }

    @Override
    public int getItemViewType(int position) {
        return (position % 2 == 0) ? 0 : 1;
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_main, parent, false));
        /*if(viewType==0){
            viewHolder.ivGalleryItem.requestLayout();
            viewHolder.ivGalleryItem.getLayoutParams().height = 200;
        } else {
            viewHolder.ivGalleryItem.requestLayout();
            viewHolder.ivGalleryItem.getLayoutParams().height = 100;
        }*/
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {
        String url = mGalleryImageList.get(position);
        Picasso.with(holder.ivGalleryItem.getContext())
                .load(TextUtils.isEmpty(url) ? null : "file://"+url)
                .transform(new BitmapTransform())
                .into(holder.ivGalleryItem);
    }

    @Override
    public int getItemCount() {
        return mGalleryImageList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivGalleryItem;

        public ViewHolder(View itemView) {
            super(itemView);
            ivGalleryItem = (ImageView) itemView.findViewById(R.id.iv_gallery_item);
        }
    }
}
